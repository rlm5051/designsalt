import React, { useState } from "react";
import { useStaticQuery, graphql } from "gatsby";
import Masonry from "react-masonry-component";
import MasonryCard from "../../../components/masonry-card/masonry-card";
import Button from "../../../components/button/button";
import BlogPostsWrapper, { PostRow, PostCol, LoadMoreButton } from "./style";
import { useBottomScrollListener } from "react-bottom-scroll-listener";
import { RelatedBlogXWrapper, RelatedBlogItem, RelatedBlogItems } from '../../../templates/templates.style';


type PostsProps = {};

const Posts: React.FunctionComponent<PostsProps> = () => {
  const Data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
        }
      }
      allStrapiArticle(sort: { fields: date, order: DESC }) {
        totalCount
        edges {
          node {
            fields {
              slug
              tags
              cloudinary {
                small
              }
            }
            title
            date(formatString: "MMM DD, YYYY")
            content
          }
        }
      }
    }
  `);

  const Posts = Data.allStrapiArticle.edges;
  const TotalPost = Data.allStrapiArticle.edges.length;

  const [state, setState] = useState({
    visibile: 8,
  });

  const [load, setload] = useState({
    loading: false,
  });

  const fetchMoreData = () => {
    setload({ loading: true });

    setTimeout(function () {
      setState((prev) => {
        return { visibile: prev.visibile + 8 };
      });
      setload({ loading: false });
    }, 1000);
  };

  useBottomScrollListener(fetchMoreData);

  return (
    <BlogPostsWrapper>
       
          <RelatedBlogXWrapper>
          <Masonry className="showcase">
            <RelatedBlogItems>
            {Posts.slice(0, state.visibile).map(({ node }: any) => {
              const title = node.title || node.fields.slug;
              return (
                <RelatedBlogItem>
                  <MasonryCard
                    title={title}
                    excerpt={node.content}
                    image={node.fields.cloudinary.small}
                    url={node.fields.slug}
                  />
                </RelatedBlogItem>
              );
            })}
            </RelatedBlogItems>
            </Masonry>
        </RelatedBlogXWrapper>
        
        <LoadMoreButton>
          {state.visibile < TotalPost ? (
            <div>
              {load.loading && (
                <Button
                  title="Load more"
                  type="submit"
                  onClick={fetchMoreData}
                  isLoading={load.loading == true ? true : false}
                  loader="Loading.."
                  style={{ borderRadius: 3, fontWeight: 700 }}
                />
              )}
            </div>
          ) : (
            <p>No more posts</p>
          )}
        </LoadMoreButton>
    </BlogPostsWrapper>
  );
};

export default Posts;
