import React, { useState } from "react";
import { Link, useStaticQuery } from "gatsby";
import _ from "lodash";
import { IoIosSearch, IoIosClose } from "react-icons/io";
import { DrawerProvider } from "../../components/drawer/drawer-context";
import SocialProfile from "../../components/social-profile/social-profile";
import Menu from "./menu";
import Drawer from "./drawer";
import SearchContainer from "../search/search";
import HeaderWrapper, {
  HeaderTop,
  TopBarWrapper,
  NavbarWrapper,
  Logo,
  NavItem,
  NavLink,
  NavItems,
  MenuWrapper,
  NavSearchButton,
  NavSearchWrapper,
  SearchCloseButton,
  NavSearchFromWrapper,
  SocialProfileWrapper,
} from "./navbar.style";
import LogoImage from "../../images/designsalt/logo.png";

type NavbarProps = {
  className?: string;
};

const MenuItems = [
  {
    label: "Home",
    url: "/",
  },
  {
    label: "Contact",
    url: "/contact",
  },
];

const Navbar: React.FunctionComponent<NavbarProps> = ({
  className,
  ...props
}) => {
  const Data = useStaticQuery(graphql`
    query {
      allStrapiArticle {
        group(field: fields___categories) {
          fieldValue
        }
      }
    }
  `);

  const Category = Data.allStrapiArticle.group;

  const [state, setState] = useState({
    toggle: false,
    search: "",
  });

  const toggleHandle = () => {
    setState({
      ...state,
      toggle: !state.toggle,
    });
  };

  // Add all classs to an array
  const addAllClasses = ["header"];

  // className prop checking
  if (className) {
    addAllClasses.push(className);
  }

  return (
    <HeaderWrapper className={addAllClasses.join(" ")} {...props}>
      <HeaderTop>
        <TopBarWrapper>
          <Logo>
            <Link to="/" rel="canonical">
              <img src={LogoImage} alt="logo" />
            </Link>
          </Logo>

          <NavItems>
            {Category.slice(0, 5).map((cat: any, index: any) => (
              <NavItem key={index}>
                <NavLink to={`/category/${_.kebabCase(cat.fieldValue)}`}>
                  {cat.fieldValue.toUpperCase()}
                </NavLink>
              </NavItem>
            ))}
          </NavItems>

          <NavItems>
            <NavSearchButton
              type="button"
              aria-label="search"
              onClick={toggleHandle}
            >
              <IoIosSearch size="23px" />
            </NavSearchButton>

            <DrawerProvider>
              <Drawer items={MenuItems} logo={LogoImage} />
            </DrawerProvider>
          </NavItems>
        </TopBarWrapper>
        <NavSearchWrapper className={state.toggle === true ? "expand" : ""}>
          <NavSearchFromWrapper>
            <SearchContainer />
            <SearchCloseButton
              type="submit"
              aria-label="close"
              onClick={toggleHandle}
            >
              <IoIosClose />
            </SearchCloseButton>
          </NavSearchFromWrapper>
        </NavSearchWrapper>
      </HeaderTop>

      {/* <NavbarWrapper className='navbar'>
        <MenuWrapper>
          <Menu items={MenuItems} />
        </MenuWrapper>
      </NavbarWrapper> */}
    </HeaderWrapper>
  );
};

export default Navbar;
