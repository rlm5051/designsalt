import * as React from "react";
import { Link, useStaticQuery, graphql } from "gatsby";
import _ from "lodash";

import FooterWrapper, {
  FooterCol,
  Logo,
  Infos,
  FooterTitle,
  FooterContent,
  Menu,
} from "./footer.style";
import LogoImage from "../../images/giftjet/logo.svg";

type FooterProps = {
  children: React.ReactNode;
};

const temp="deploy testing";

const Footer: React.FunctionComponent<FooterProps> = ({
  children,
  ...props
}) => {
  const Data = useStaticQuery(graphql`
    query {
      allStrapiArticle {
        group(field: fields___categories) {
          fieldValue
        }
      }
    }
  `);
  const Category = Data.allStrapiArticle.group;

  const MenuItems = [
    {
      label: "Affiliate Disclosure",
      url: "/affiliate-disclosure",
    },
    {
      label: "Contact",
      url: "/contact",
    },
    ...Category.map((cat: any) => ({
      url: `/category/${_.kebabCase(cat.fieldValue)}`,
      label: cat.fieldValue,
    })),
  ];

  const divLength = Math.floor(MenuItems.length / 4);

  return (
    <FooterWrapper {...props}>
      <FooterCol>
        <Logo>
          <Link to="/">
            <img src={LogoImage} style={{ height: "15px" }} alt="logo" />
          </Link>
        </Logo>
        <br />
        <Infos>
          Copyright &copy;&nbsp;
          <a href="https://redq.io/">giftjet.co</a>
        </Infos>
      </FooterCol>
      {[0, 1, 2, 3].map((group: any, index: any) => (
        <FooterCol key={index + 3}>
          <FooterContent>
            {MenuItems.slice(group * divLength, (group + 1) * divLength).map(
              (item: any, index: any) => (
                <Menu key={index} to={item.url}>
                  {item.label}
                </Menu>
              )
            )}
            {divLength * 4 + group < MenuItems.length && (
              <Menu key={100} to={MenuItems[divLength * 4 + group].url}>
                {MenuItems[divLength * 4 + group].label}
              </Menu>
            )}
          </FooterContent>
        </FooterCol>
      ))}

      {/* <FooterCol>
        <FooterTitle>Follow Us</FooterTitle>

        <SocialProfile items={SocialLinks} />
      </FooterCol> */}
    </FooterWrapper>
  );
};

export default Footer;
