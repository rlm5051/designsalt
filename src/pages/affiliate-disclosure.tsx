import React from "react";
import Layout from "../containers/layout";
import SEO from "../components/seo";
import Contact from "../containers/contact";

const AffiliateDisclosure = () => {
  return (
    <Layout>
      <SEO title="Affiliate Disclosure" description="Affiliate Page" />
      <p>
        Disclosure: This is a professional review blog which gets compensated
        for the products reviewed by the companies who produce them. All of the
        products are tested thoroughly and high grades are received only by the
        best ones. I am an independent blogger and the reviews are done based on
        my own opinions.
      </p>
      <Contact />
    </Layout>
  );
};

export default AffiliateDisclosure;
