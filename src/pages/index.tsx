import React from "react";
import { graphql } from "gatsby";
import Layout from "../containers/layout";
import PersonalBlog from "../containers/home";
import SEO from "../components/seo";

const HomePage = (props: any) => {
  const { data } = props;

  return (
    <Layout fullWidth>
      <SEO
        title={"GiftJet | The Best Gift Ideas & Gift Guides, From Experts "}
        description={data.site.siteMetadata.description}
        noTemplate={true}
      />
      <PersonalBlog />
    </Layout>
  );
};

export default HomePage;

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
        description
      }
    }
  }
`;
