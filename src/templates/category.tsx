import React from "react";
import { graphql } from "gatsby";
import Layout from "../containers/layout";
// import PostCard from '../components/post-card/post-card';
import Masonry from "react-masonry-component";
import MasonryCard from "../components/masonry-card/masonry-card";
import SEO from "../components/seo";
import {
  TagPostsWrapper,
  TagPageHeading,
  TagName,
  PostRow,
  CategoryPostCol,
} from "./templates.style";
const temp = "heelo";

const Category = ({ pageContext, data }: any) => {
  const { category } = pageContext;
  const { edges, totalCount } = data.allStrapiArticle;

  return (
    <Layout>
      <SEO
        title={category}
        description={`A collection of ${totalCount} posts`}
      />

      <TagPostsWrapper>
        <TagPageHeading>
          <TagName>{category}</TagName>
          {`A collection of ${totalCount} post`}
        </TagPageHeading>
        <PostRow>
          <Masonry className="showcase">
            {edges.map(({ node, index }: any) => (
              <CategoryPostCol key={node.fields.slug}>
                <MasonryCard
                  title={node.title}
                  excerpt={node.content}
                  image={node.fields.cloudinary.small}
                  url={node.fields.slug}
                />
              </CategoryPostCol>
            ))}
          </Masonry>
        </PostRow>
      </TagPostsWrapper>
    </Layout>
  );
};

export default Category;

export const pageQuery = graphql`
  query($category: String) {
    allStrapiArticle(
      limit: 1000
      sort: { fields: [date], order: DESC }
      filter: { fields: { categories: { in: [$category] } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
            categories
            cloudinary {
              small
            }
          }
          date(formatString: "DD [<span>] MMMM [</span>]")
          title
          content
        }
      }
    }
  }
`;
