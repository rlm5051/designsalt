import React from "react";
import { graphql } from "gatsby";
import Layout from "../containers/layout";
import PostCard from "../components/post-card/post-card";
import SEO from "../components/seo";
import { TagPostsWrapper, TagPageHeading, TagName } from "./templates.style";

const Tags = ({ pageContext, data }: any) => {
  const { tag } = pageContext;
  const { edges, totalCount } = data.allStrapiArticle;
  
  return (
    <Layout>
      <SEO title={tag} description={`A collection of ${totalCount} post`} />

      <TagPostsWrapper>
        <TagPageHeading>
          <TagName>{tag}</TagName>
          {`A collection of ${totalCount} post`}
        </TagPageHeading>
        {edges.map(({ node, index }: any) => (
          <PostCard
            key={node.fields.slug}
            title={node.title}
            url={node.fields.slug}
            excerpt={node.content}
            date={node.date}
            tags={node.fields.tags}
          />
        ))}
      </TagPostsWrapper>
    </Layout>
  );
};

export default Tags;

export const pageQuery = graphql`
  query($tag: String) {
    allStrapiArticle(
      limit: 1000
      sort: { fields: [date], order: DESC }
      filter: { fields: { tags: { in: [$tag] } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
            tags
          }
          date(formatString: "DD [<span>] MMMM [</span>]")
          title
          content
        }
      }
    }
  }
`;

/*
 cover {
  childImageSharp {
    fluid(maxWidth: 570, quality: 100) {
      ...GatsbyImageSharpFluid_withWebp_tracedSVG
    }
  }
} */
