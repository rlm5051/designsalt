import * as React from "react";
import { graphql } from "gatsby";
import Masonry from "react-masonry-component";
import MasonryCard from "../components/masonry-card/masonry-card";
import Pagination from "../components/pagination/pagination";
import Layout from "../containers/layout";
import SEO from "../components/seo";
import { BlogPostsWrapper, PostRow, PostCol } from "./templates.style";

const BlogList = (props: any) => {
  const { data } = props;
  const Posts = data.allStrapiArticle.edges;
  const { currentPage, numPages } = props.pageContext;
  const isFirst = currentPage === 1;
  const isLast = currentPage === numPages;
  const prevPage =
    currentPage - 1 === 1 ? "/page/1" : `/page/${(currentPage - 1).toString()}`;
  const nextPage = `/page/${(currentPage + 1).toString()}`;
  const PrevLink = !isFirst && prevPage;
  const NextLink = !isLast && nextPage;

  return (
    <Layout>
      <SEO title={`Page ${currentPage}`} />
      <BlogPostsWrapper>
        <PostRow>
          <Masonry className="showcase">
            {Posts.map(({ node }: any) => {
              return (
                <PostCol key={node.fields.slug}>
                  <MasonryCard
                    title={node.title || node.fields.slug}
                    image={node.fields.cloudinary.large}
                    excerpt={node.content}
                    url={node.fields.slug}
                    date={node.date}
                    tags={node.fields.tags}
                    readTime={"node.fields.readingTime.text"}
                  />
                </PostCol>
              );
            })}
          </Masonry>
        </PostRow>

        <Pagination
          prevLink={PrevLink}
          nextLink={NextLink}
          currentPage={`${currentPage}`}
          totalPage={`${numPages}`}
        />
      </BlogPostsWrapper>
    </Layout>
  );
};

export default BlogList;

export const pageQuery = graphql`
  query($skip: Int!, $limit: Int!) {
    site {
      siteMetadata {
        title
      }
    }
    sitePage {
      path
    }
    allStrapiArticle(
      sort: { fields: date, order: DESC }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          fields {
            slug
            tags
            cloudinary {
              large
            }
          }
          date(formatString: "DD [<span>] MMMM [</span>]")
          title
          content
        }
      }
    }
  }
`;

// cover {
//   childImageSharp {
//     fluid(maxWidth: 1170, quality: 90) {
//       ...GatsbyImageSharpFluid_withWebp_tracedSVG
//     }
//   }
// }
