import React from "react";
import { graphql, Link } from "gatsby";
import _ from "lodash";
import Layout from "../containers/layout";
import SEO from "../components/seo";
import PostCard from "../components/post-card/post-card";
import ProductCard from "../components/product-card/product-card";
import PostDetails from "../components/post-details/post-details";
import ReactMarkdown from "react-markdown";

import {
  BlogPostDetailsWrapper,
  RelatedPostWrapper,
  RelatedPostItems,
  RelatedPostTitle,
  RelatedPostItem,
  ProductPostDetailsWrapper,
  RelatedProductWrapper,
  RelatedProductItems,
  RelatedProductTitle,
  RelatedProductItem,
} from "./templates.style";

const BlogPostTemplate = ({ pageContext, ...props }: any) => {
  const post = props.data.strapiArticle;
  const { edges } = props.data.allStrapiArticle;
  const { products } = post.fields;
  return (
    <Layout fullWidth={true}>
      <SEO title={post.title} description={post.metaDescription} />
      <BlogPostDetailsWrapper>
        <PostDetails
          title={post.title}
          date={post.date}
          categories={post.fields.categories}
          preview={post.fields.cloudinary.large}
          description={post.content}
        />
        <h3 style={{color: "black", 
        fontSize: "36px",letterSpacing: "1.3px",  fontWeight: "300", textAlign: "center", paddingTop: '40px'}}>
          Top trending gifts, direct from brands</h3>
          <div
            style={{paddingLeft: '100px', paddingRight:'100px'}}
          >
            <ReactMarkdown source={post.content} />
          </div>
        
        {products.length !== 0 && (
          <RelatedProductWrapper>
            {/* <RelatedProductTitle>
              {post.productHeader ? post.productHeader : "Best products"}
            </RelatedProductTitle> */}
            <RelatedProductItems>
              {products.map((product: any) => (
                <RelatedProductItem key={product.id}>
                  <ProductCard
                    title={product.title}
                    editor={product.editor}
                    excerpt={product.description}
                    image={product.images.small}
                    url={product.productLink}
                    price={product.price}
                  />
                </RelatedProductItem>
              ))}
            </RelatedProductItems>
          </RelatedProductWrapper>
        )}
      </BlogPostDetailsWrapper>

      {edges.length !== 0 && (
        <RelatedPostWrapper>
          <RelatedPostTitle>Related Posts</RelatedPostTitle>
          <RelatedPostItems>
            {edges.map(({ node }: any) => (
              <RelatedPostItem key={node.fields.slug}>
                <PostCard
                  title={node.title || node.fields.slug}
                  excerpt={node.content}
                  image={node.fields.cloudinary.small}
                  url={node.fields.slug}
                />
              </RelatedPostItem>
            ))}
          </RelatedPostItems>
        </RelatedPostWrapper>
      )}
    </Layout>
  );
};

export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!, $tag: [String!]) {
    site {
      siteMetadata {
        siteUrl
      }
    }
    strapiArticle(slug: { eq: $slug }) {
      fields {
        slug
        tags
        categories
        cloudinary {
          large
        }
        products {
          images {
            medium
            small
            thumbnail
          }
          description
          productLink
          id
          title
          price
        }
      }
      productHeader
      title
      content
      metaDescription
      date(formatString: "DD MMM, YYYY")
    }
    allStrapiArticle(
      limit: 3
      sort: { fields: date, order: DESC }
      filter: { fields: { tags: { in: $tag }, slug: { ne: $slug } } }
    ) {
      edges {
        node {
          fields {
            categories
            tags
            slug
            cloudinary {
              small
            }
          }
          title
        }
      }
    }
  }
`;
