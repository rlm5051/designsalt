import * as React from "react";
import { Link } from "gatsby";
import _ from "lodash";
import Img from "gatsby-image";
import ReactMarkdown from "react-markdown";

import {
  MasonryCardWrapper,
  PostPreview,
  PostDetails,
  PostTitle,
  PostExcerpt,
  ReadMore,
} from "./masonry-card.style";

interface MasonryCardProps {
  image?: any;
  title: string;
  excerpt: string;
  url: string;
  date?: string;
  tags?: [];
  className?: string;
  imageType?: "fixed" | "fluid";
  readTime?: string;
}

const MasonryCard: React.FunctionComponent<MasonryCardProps> = ({
  image,
  title,
  excerpt,
  url,
  date,
  tags,
  className,
  imageType,
  readTime,
  ...props
}) => {
  // Add all classs to an array
  const addAllClasses = ["mesonry_card"];

  // className prop checking
  if (className) {
    addAllClasses.push(className);
  }

  return (
    <MasonryCardWrapper className={addAllClasses.join(" ")} {...props}>
      <Link to={`/${url}`}>
      <PostPreview background={image} className="post_preview">
      </PostPreview>
      <PostTitle className="post_title">
        {title}
      </PostTitle>
      </Link>

      {/* <PostDetails className="post_details">
        
        {excerpt && (
          <PostExcerpt>
            <ReactMarkdown source={excerpt.substring(0, 300).concat("...")} />
          </PostExcerpt>
        )}
        <ReadMore>
          <Link to={`/${url}`}>Read More</Link>
        </ReadMore>
      </PostDetails> */}
    </MasonryCardWrapper>
  );
};

MasonryCard.defaultProps = {
  imageType: "fluid",
};

export default MasonryCard;
