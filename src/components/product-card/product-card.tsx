import * as React from "react";
import { Link } from "gatsby";
import _ from "lodash";
import { useStaticQuery, graphql } from "gatsby";
import ReactMarkdown from "react-markdown";
import ReactTooltip from 'react-tooltip';

import {
  ProductCardWrapper,
  ProductPreview,
  ProductDetails,
  ProductExcerpt,
  ProductTitle,
  PersonInfo,
  ProductContent,
  ReadMore,
} from "./product-card.style";


const ProductCard = ({
  image,
  title,
  url,
  excerpt,
  date,
  editor,
  price,
  tags,
  className,
  imageType,
  ...props
}) => {
  const data = useStaticQuery(graphql`
    query {
      allStrapiEditor {
        edges {
          node {
            firstName
            lastName
            strapiId
            fields {
              image
            }
          }
        }
      }
    }
  `);

  const dataset = data.allStrapiEditor.edges;

  const editorInfo = _.find(dataset, (node) => {
    return node.strapiId == editor;
  });
  // Add all classs to an array
  const addAllClasses = ["product_card"];

  // className prop checking
  if (className) {
    addAllClasses.push(className);
  }

  return (
    <ProductCardWrapper className={addAllClasses.join(" ")} {...props}>
      {image == null ? null : (
        <ProductPreview className="product_preview">
          <Link to={`${url}`}>
            <img
              src={image}
              alt="post-preview"
            />
          </Link>
        </ProductPreview>
      )}

      <ProductDetails className="product_details">
        <div style={{display: "flex", justifyContent: "space-between"}}>
          <div>
          <ProductTitle className="product_title">
            <Link to={`${url}`}>{title}</Link>
            
          </ProductTitle>
          
          {excerpt && (
          <ProductExcerpt>
            <ReactMarkdown source={excerpt} />
          </ProductExcerpt>
        )}
          </div>
          
          <span style={{color: "gray"}}>${price}</span>
        </div>
        
        <div style={{display: "flex", justifyContent: "space-between", margin: "10px 10px 0px 10px"}}>
          <div className="text-center" style={{width: "50px"}}>
            <div style={{width: "35px", margin: "0 auto"}}>
            <img
              style={{
                height: "35px",
                width:"35px",
                border: "1px solid rgb(245, 245, 245)",
                borderRadius: "50%",
              }}
              data-tip={editorInfo.node.firstName + editorInfo.node.lastName}
              alt={editorInfo.node.firstName + editorInfo.node.lastName}
              src={editorInfo.node.fields.image}
            />
              <ReactTooltip 
                place="bottom" 
                backgroundColor="#fff"
                arrowColor="#fff" 
                textColor="#000" 
                className="tooltip-name" 
                type='light'
              />
            </div>
            
          </div>
        <ProductContent>
         
        </ProductContent>
        {/* {excerpt && (
          <ProductExcerpt>
            <ReactMarkdown source={excerpt} />
          </ProductExcerpt>
        )} */}

        <div style={{width: "70px"}}>
        <ReadMore>
          <a href={`${url}`} target="_blank" >Buy</a>
        </ReadMore>
        </div>
        </div>
      </ProductDetails>

    </ProductCardWrapper>
  );
};

export default ProductCard;
