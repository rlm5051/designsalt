import styled from "styled-components";
import { themeGet } from "@styled-system/theme-get";

// box-shadow: 0px 5px 14px rgba(44, 67, 81, 0.13), 0px 0px 4px rgba(44, 67, 81, 0.02);
export const ProductCardWrapper = styled.div`
  border: 1px solid #e9e9e9;
  border-radius: 8px;
  position: relative;
  height: 100%;
  border-radius: 3px;
  overflow: hidden;
  display: flex;
  flex-direction: column;
`;

export const ProductPreview = styled.div`
  position: relative;
  overflow: hidden;
  flex-shrink: 0;
  margin: 0 auto;
  img {
    width: 100%;
    padding-bottom: 10px
    border-radius: 3px;
    
  }
  a {
    display: block;
    transition: 0.25s ease-in-out;
  }
`;

export const PersonInfo = styled.div`
  width: 100%;
  height: 100%;
`;

export const ProductDetails = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  padding: 10px 10px;
`;

export const ReadMore = styled.div`
padding-top: 2px;
padding-bottom: 20px;
padding-left: 12px;
  a {
    border: none;
    display: inline-block;
    padding: 5px 16px;
    vertical-align: middle;
    overflow: hidden;
    text-decoration: none;
    color: inherit;
    background-color: inherit;
    text-align: center;
    cursor: pointer;
    white-space: nowrap;
    color: #FFFFFF;
    background-color: #fc604e;
    border-radius: 3px;
    font-weight: 800;
    transition: 0.15s ease-in-out;
    @media (max-width: 990px) {
      font-size: 14px;
    }
    &:hover {
      box-shadow: none;
      background-color: #f25a49;
    }
  }
`;

export const ProductContent = styled.div`
  span {
    text-decoration: underline;
    color: rgb(93, 113, 255);
    font-size: 16px;
    letter-spacing: 0px;
    line-height: 1.5;
    font-weight: 800;
    cursor: pointer;
    &:hover {
      color: rgb(193, 103, 255)!important;
    }
  }
`;


export const ProductTitle = styled.h6`
  font-size: 18px;
  font-weight: 400;
  color: ${themeGet("colors.textColor", "#292929")};
  line-height: 1.5;
  overflow: hidden;
  text-overflow: ellipsis;
  margin-bottom: 10px;
  a {
    color: ${themeGet("colors.textColor", "#292929")};
    overflow: hidden;
   
  }
`;

export const ProductExcerpt = styled.p`
  font-size: 14px;
  font-weight: 300;
  color: ${themeGet("colors.textLightColor", "#5e5e5e")};
  line-height: 1.5;
  margin-bottom: 20px;
  margin-top: auto;
  text-align: center;
  @media (max-width: 990px) {
    font-size: 15px;
  }
`;
