import CircularStdBookTTF from "../../static/fonts/Circular-Std-Book.ttf";
import CircularStdBookWOFF from "../../static/fonts/Circular-Std-Book.woff";
import CircularStdMediumTTF from "../../static/fonts/Circular-Std-Medium.ttf";
import CircularStdMediumWOFF from "../../static/fonts/Circular-Std-Medium.woff";
import FreightSansProBookRegularTTF from "../../static/fonts/FreightSansProBook-Regular.ttf";
import FreightSansProBookRegularWOFF from "../../static/fonts/FreightSansProBook-Regular.woff";
import FreightSansProLightRegularTTF from "../../static/fonts/FreightSansProLight-Regular.ttf";
import FreightSansProLightRegularWOFF from "../../static/fonts/FreightSansProLight-Regular.woff";
import FreightSansProMediumRegularTTF from "../../static/fonts/FreightSansProMedium-Regular.ttf";
import FreightSansProMediumRegularWOFF from "../../static/fonts/FreightSansProMedium-Regular.woff";
import FreightSansProSemiboldRegularTTF from "../../static/fonts/FreightSansProSemibold-Regular.ttf";
import FreightSansProSemiboldRegularWOFF from "../../static/fonts/FreightSansProSemibold-Regular.woff";

const fontFiles = {
  CircularStdBookTTF,
  CircularStdBookWOFF,
  CircularStdMediumTTF,
  CircularStdMediumWOFF,
  FreightSansProBookRegularTTF,
  FreightSansProBookRegularWOFF,
  FreightSansProLightRegularTTF,
  FreightSansProLightRegularWOFF,
  FreightSansProMediumRegularTTF,
  FreightSansProMediumRegularWOFF,
  FreightSansProSemiboldRegularTTF,
  FreightSansProSemiboldRegularWOFF,
};

export default fontFiles;
