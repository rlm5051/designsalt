import * as React from "react";
import Img from "gatsby-image";
import { Link } from "gatsby";
import _ from "lodash";
import ReactMarkdown from "react-markdown";
import {
  PostDetailsWrapper,
  PostTitle,
  PostDate,
  PostCategory,
  PostPreview,
  PostDescriptionWrapper,
  PostDescription,
  PostTags,
  WithAdsWrapper,
  AdsWrapper,
} from "./post-details.style";

type PostDetailsProps = {
  title: string;
  date?: string;
  preview?: any;
  description: any;
  tags?: [];
  categories?: [];
  className?: string;
  imagePosition?: "left" | "top";
};

const PostDetails = ({
  title,
  date,
  preview,
  description,
  tags,
  categories,
  className,
  imagePosition,
  ...props
}) => {
  const addClass: string[] = ["post_details"];

  if (imagePosition == "left") {
    addClass.push("image_left");
  }

  if (className) {
    addClass.push(className);
  }

  return (
  
      <PostDetailsWrapper {...props} className={addClass.join(" ")}>
        {preview == null ? null : (
          <PostPreview background={preview} className="post_preview" >
              <PostDate>THE 2020 HOLIDAY</PostDate>
              <PostTitle>{title}</PostTitle>
          </PostPreview>
        )}
          
        {/* <PostDescriptionWrapper className="post_des_wrapper">
          <PostDescription className="post_des">
            <ReactMarkdown source={description} />
          </PostDescription>
          {tags == null ? null : (
            <PostTags>
              {tags.map((tag, index) => (
                <Link key={index} to={`/tags/${_.kebabCase(tag)}/`}>
                  {`#${tag}`}
                </Link>
              ))}
            </PostTags>
          )}
        </PostDescriptionWrapper> */}
      </PostDetailsWrapper>
     
  );
};

PostDetails.defaultProps = {
  imagePosition: "top",
};

export default PostDetails;
