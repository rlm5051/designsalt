const path = require(`path`);
const _ = require("lodash");
const { createFilePath } = require(`gatsby-source-filesystem`);
const crypto = require(`crypto`);

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;

  const blogPost = path.resolve(`./src/templates/blog-post.tsx`);
  const blogList = path.resolve(`./src/templates/blog-list.tsx`);
  const tagTemplate = path.resolve(`./src/templates/tags.tsx`);
  const categoryTemplate = path.resolve(`./src/templates/category.tsx`);

  return graphql(
    `
      {
        allStrapiArticle(limit: 1000, sort: { fields: date, order: DESC }) {
          edges {
            node {
              fields {
                categories
                tags
                slug
                cloudinary {
                  thumbnail
                }
              }
              title
              content
            }
          }
        }
      }
    `
  ).then((result) => {
    if (result.errors) {
      throw result.errors;
    }

    // Create blog posts pages.
    const posts = result.data.allStrapiArticle.edges;

    posts.forEach((post, index) => {
      const previous =
        index === posts.length - 1 ? null : posts[index + 1].node;
      const next = index === 0 ? null : posts[index - 1].node;

      createPage({
        path: post.node.fields.slug,
        component: blogPost,
        context: {
          slug: post.node.fields.slug,
          previous,
          next,
          tag: post.node.fields.tags,
          category: post.node.fields.categories,
        },
      });
    });

    // Create blog post list pages
    const postsPerPage = 6;
    const numPages = Math.ceil(posts.length / postsPerPage);

    let tags = [];
    _.each(posts, (edge) => {
      if (_.get(edge, "node.fields.tags")) {
        tags = tags.concat(edge.node.fields.tags);
      }
    });
    tags = _.uniq(tags);

    // Category pages:
    let categories = [];
    // Iterate through each post, putting all found tags into `tags`
    _.each(posts, (edge) => {
      if (_.get(edge, "node.fields.categories")) {
        categories = categories.concat(edge.node.fields.categories);
      }
    });

    // Eliminate duplicate tags
    categories = _.uniq(categories);

    // Make tag pages
    categories.forEach((category) => {
      createPage({
        path: `/category/${_.kebabCase(category)}/`,
        component: categoryTemplate,
        context: {
          category,
        },
      });
    });

    return null;
  });
};

const digest = (data) => {
  return crypto.createHash(`md5`).update(JSON.stringify(data)).digest(`hex`);
};

const linkProductToNode = (products) => {
  return products.map((product) => {
    let imageURL = product.image ? product.image.url : null;

    let large = null;
    let medium = null;
    let small = null;
    let thumbnail = null;

    if (imageURL) {
      let parentURL = imageURL.split(/\/[^\/]*$/)[0];
      let subURL = imageURL.substring(parentURL.length + 1, imageURL.length);
      large = parentURL + "/large_" + subURL;
      medium = parentURL + "/medium_" + subURL;
      small = parentURL + "/small_" + subURL;
      thumbnail = parentURL + "/thumbnail_" + subURL;
    }

    return {
      ...product,
      images: {
        medium,
        small,
        thumbnail,
      },
      // editorImage: editorImageURL,
      productLink: product.product_link,
    };
  });
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;

  if (node.internal.type === "StrapiArticle") {
    let imageURL = node.cover ? node.cover.url : null;

    let large = null;
    let medium = null;
    let small = null;
    let thumbnail = null;

    if (imageURL) {
      let parentURL = imageURL.split(/\/[^\/]*$/)[0];
      let subURL = imageURL.substring(parentURL.length + 1, imageURL.length);
      large = parentURL + "/large_" + subURL;
      medium = parentURL + "/medium_" + subURL;
      small = parentURL + "/small_" + subURL;
      thumbnail = parentURL + "/thumbnail_" + subURL;
    }

    const products = linkProductToNode(node.products);

    createNodeField({
      node,
      name: "slug",
      value: node.slug,
    });

    createNodeField({
      node,
      name: "cloudinary",
      value: {
        large,
        medium,
        small,
        thumbnail,
      },
    });

    createNodeField({
      node,
      name: "products",
      value: products,
    });

    createNodeField({
      node,
      name: "categories",
      value: node.categories.map((category) => category.name),
    });
    createNodeField({
      node,
      name: "tags",
      value: node.tags.map((tag) => tag.name),
    });
  }

  if (node.internal.type === "StrapiEditor") {
    let imageURL = null;
    if (node.image) imageURL = node.image.url;
    if (node.photo) imageURL = node.photo.url;
    let thumbnail = null;

    if (imageURL) {
      let parentURL = imageURL.split(/\/[^\/]*$/)[0];
      let subURL = imageURL.substring(parentURL.length + 1, imageURL.length);
      thumbnail = parentURL + "/thumbnail_" + subURL;
    }

    createNodeField({
      node,
      name: "image",
      value: thumbnail,
    });
  }
};

// for React-Hot-Loader: react-🔥-dom patch is not detected
exports.onCreateWebpackConfig = ({ getConfig, stage }) => {
  const config = getConfig();
  if (stage.startsWith("develop") && config.resolve) {
    config.resolve.alias = {
      ...config.resolve.alias,
      "react-dom": "@hot-loader/react-dom",
    };
  }
};
